import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import serverRequests from './requests/server-requests';
import CurrencyCard from './components/fx-card/CurrencyCard'
import FxCardsList from './components/fx-cards-list/FxCardsList'


class App extends Component {
  constructor(props) {
    super(props);
    this.state = { serverResponse: "" };
    // this.requests = async function (){return await FxCardsList()};
    // this.requests = serverRequests(this);
  }

  componentWillMount() {
    // FxCardsList()
    // console.log('FROM APP');
    // serverRequests(this).getCurrencies();
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            THIS WILL BE MY AWESOME FX APPLICATION
          </p>
          <p>HERE IS FIRST PROVE: </p>
          <FxCardsList />

        </header>
      </div>
    );
  }
}

export default App;
