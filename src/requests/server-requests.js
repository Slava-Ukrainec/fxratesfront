import serverUrl from './config'

function testConnection() {
    fetch(`${serverUrl}/testAPI`)
        .then(res => res.text())
        .then(res => this.setState({serverResponse: res}));
}

// function getCurrencies() {
//     fetch(`${serverUrl}/currencies`)
//         .then(res => res.text())
//         .then(res => this.setState({serverResponse: JSON.parse(res)}));
// }



function getCurrencies() {
     return fetch(`${serverUrl}/currencies`)
        .then(res => res.text())
        .then(res => JSON.parse(res))
}


// const serverRequests = function (context) {
//     return {
//         testConnection: testConnection.bind(context),
//         getCurrencies: getCurrencies.bind(context)
//     }
// };

const serverRequests =  {
        testConnection,
        getCurrencies
    };

export default serverRequests