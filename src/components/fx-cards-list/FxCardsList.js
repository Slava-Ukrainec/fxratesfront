import React, {Component} from 'react';
import serverRequests from '../../requests/server-requests'

function getCityName(cityId, citiesObj) {
    return citiesObj[cityId]
}

function getCurrencyName(curId, curenciesObj) {
    return curenciesObj[curId]
}

function FxTableRow(props) {
    const {output} = props;
    return (
        <tr>
            <td>{output.city}</td>
            <td>{output.currencyName}</td>
            <td>{output.currencyId}</td>
            <td>{output.ask}</td>
            <td>{output.bid}</td>
            <td>{output.title}</td>
            <td>{output.adress}</td>
        </tr>
    )
}

class FxCardsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fxRates: [],
            number: 123
        };
    }

    componentDidMount() {
        serverRequests.getCurrencies().then(data => {
            const {currencies, cities, organizations} = data;
            console.log(data);
            const ratesMatrix = [];
            organizations.forEach(item => {
                for (let key in item.currencies) {
                    const obj = {};
                    obj.city = getCityName(item.cityId, cities);
                    obj.title = item.title;
                    obj.currencyId = key;
                    obj.currencyName = getCurrencyName(key, currencies);
                    obj.ask = item.currencies[key].ask;
                    obj.bid = item.currencies[key].bid;
                    obj.link = item.link;
                    obj.adress = item.address;
                    obj.key = `${key}${item.id}`;
                    ratesMatrix.push(obj);
                }
            });

            this.setState({fxRates: ratesMatrix})
        });
        // let data = await serverRequests.getCurrencies();
        // const {currencies,cities,organizations}=data;
        // const ratesMatrix =[];
        // organizations.forEach(item=>{
        //     for (let key in item.currencies) {
        //         const obj = {};
        //         obj.city = getCityName(item.cityId, cities);
        //         obj.title = item.title;
        //         obj.currencyId = key;
        //         obj.currencyName = getCurrencyName(key, currencies);
        //         obj.ask = item.currencies[key].ask;
        //         obj.bid = item.currencies[key].bid;
        //         obj.link = item.link;
        //         obj.adress = item.address;
        //         ratesMatrix.push(obj);
        //     }
        // });
        // this.setState({fxRates: ratesMatrix})
    }

    render() {
        console.log('rework');
        // this.prepareCurrencies();
        const {fxRates, number} = this.state;
        // console.log(fxRates);

        // const rows = ratesMatrix.map(item=>{
        //     return 0
        //     return <tr>

        //     </tr>
        // });
        // console.log(rows);
        return (<table>
                <thead>
                <tr>
                    <th>CITY</th>
                    <th>CURRENCY</th>
                    <th>CURRENCY CODE</th>
                    <th>ASK</th>
                    <th>BID</th>
                    <th>OUTLET</th>
                    <th>ADRESS</th>
                </tr>
                </thead>
                <tbody>
                {fxRates.length > 0 && fxRates.map(item => <FxTableRow key={item.key} output={item}/>)}
                </tbody>
            </table>
        );
    }
}

// city --- title --- currency --- ask --- bid --- link

// function getCur() {
//     let fun = serverRequests(this);
//     return new Promise ((resolve,reject)=>fun.getCurrencies().then(resolve)).;
//     // fun.getCurrencies().then(result=>console.log(result));
//     // return new Promise ((resolve,reject)=> {fun.getCurrencies().then(resolve)})
//     // console.log(x);
//     // x.then(result => console.log(result));
//     // console.log(x);
//
// }
//
// const result = getCur();
// console.log(result);
//
export default FxCardsList