import React, {Component, Fragment} from 'react';

class CurrInput extends Component {
    state = {
        value: 0
    };

    onChange =(evt)=>{
        this.props.curChange(this.props.id, evt.target.value)
    };
    render() {
        const{id,curChange}=this.props;
        const {value} = this.state;
        return (
            <div style={{margin:"10px"}}>
                <span>{id}:</span>
                <input type="text" onChange={this.onChange} value={this.props.value}/>
            </div>
        )
    }
}

class CurrencyCard extends Component {
    state = {
        USD: 0,
        UAH: 0,
        EUR: 0,
        rates:{
            USD: 27,
            UAH: 1,
            EUR: 30
        }
    };
    onCurrencyChange = (id,number) => {
        const localState = Object.assign({},this.state);
        localState[id] = number;
        this.setState(localState);
        console.log('onCurrency change WORKS');
        console.log('new Value:', number)
    };
    render() {
        const {base} = this.props;
        const {USD,UAH,EUR} = this.state;
        return <Fragment>
            <h1>Currency Exchange</h1>
            <h2>Base currency: <span>{base}</span></h2>
            <div>
                <CurrInput curChange={this.onCurrencyChange} id='UAH' value={UAH}/>
                <CurrInput curChange={this.onCurrencyChange} id='USD' value={UAH/27}/>
                <CurrInput curChange={this.onCurrencyChange} id='EUR' value={UAH/30}/>
            </div>
        </Fragment>
    }
}

export default CurrencyCard